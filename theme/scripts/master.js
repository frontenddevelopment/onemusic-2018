jQuery(document).ready(function($) {
    oneMusic.init();
});

var oneMusic = (function() {
    'use strict';

    function init() {
        $('body').prepend('<div class="loader">' +
            '<div class="container">' +
            '    <div class="parts">' +
            '        <div class="logo-container"><img src="theme/images/logo-p1.png" class="p1" alt="">' +
            '        <img src="theme/images/logo-p2.png" class="p2" alt="">' +
            '        <img src="theme/images/logo-p3.png" class="p3" alt=""></div>' +
            '    </div>' +
            '</div>' +
            '</div>');

        setTimeout(function() {
            $('.loader').fadeOut('fast');
            $('body').addClass('loaded');
            smoothScroll();
        }, 4500);

        resizeFunction();
        getBeDiscovered();
        getMustWatch();
        getHomePlaylists();
        getMustRead();
        getFeaturedEvents();
        getDontMissOut();
        getBeDiscoveredSplash();
        getSuccessStories();
        getLivestream();
        getDigiconLive();
        getDigiconReplay();
        getDigiconChat();
        getDigiconPoll();

        if ($('.section-side aside .sticky').length > 0) {
            $(".section-side aside .sticky").stick_in_parent();
        }
        if ($('.section-side .article-content .sticky').length > 0) {
            $(".section-side .article-content .sticky").stick_in_parent();
        }
        if ($('.digicon.side-container').length > 0) {
            // videoSide();
        }
        if ($('.scroller-status').length > 0) {
            $('.article-list-container').infiniteScroll({
                path: '#next',
                append: '.content',
                status: '.scroller-status',
                hideNav: '.pagination',
            });
        }
        if ($('.article-list-container .slider-gallery').length > 0) {
            gallerySlide();
        }
        if ($('.video-sliders').length > 0) {
            watchNowSlider();
        }
        if ($('.submit-form').length > 0) {
            var formType = getUrlParameter('form');

            $('.join-as input').attr('value', formType);
            $('.' + formType).fadeIn('fast');
        }

        $(window).on('resize orientationchange', function() {
            resizeFunction();
            $('.spotlight .flex').slick('resize');
        });

        $(document).on('append.infiniteScroll', function(event, response, path, items) {
            if ($('.dark').length > 0) {
                $('.section-side .buttons .mode .label').text('Light Mode');
                $('.section-side .buttons .mode a').addClass('go-light');
                $('.section-side .buttons .mode a').removeClass('go-dark');
            }
        });

        $(document).on('.gallery-page append.infiniteScroll', function(event, response, path, items) {
            gallerySlide();
        });

        $(document).on('click', 'nav .burger:not(.opened)', function(event) {
            event.preventDefault();
            /* Act on the event */
            $('.nav-side').animate({ "left": '+=350' });
            $('nav').animate({ "margin-left": '+=350' });
            $(this).addClass('opened');
            $('nav .burger i').addClass('fa-times');
            $('nav .burger i').removeClass('fa-bars');
        });

        $(document).on('click', 'nav .burger.opened', function(event) {
            event.preventDefault();

            $('.nav-side').animate({ "left": '-=350' });
            $('nav').animate({ "margin-left": '-=350' });
            $(this).removeClass('opened');
            $('nav .burger i').addClass('fa-bars');
            $('nav .burger i').removeClass('fa-times');
        });

        $(document).on('click', '.parent:not(.expand)', function(event) {
            $('.parent.expand .sub').slideToggle('fast');
            $('.parent.expand').removeClass('expand');

            $(this).find('.sub').slideToggle('fast');
            $(this).toggleClass('expand');
        });

        $(document).on('click', '.parent.expand', function(event) {
            $(this).find('.sub').slideToggle('fast');
            $(this).toggleClass('expand');
        });

        $(document).on('click', '.digicon .tabs a', function(event) {
            event.preventDefault();
            /* Act on the event */

            $('.digicon .tabs a').removeClass('active');
            $(this).addClass('active');
            $('.digicon .tab-contents .content').hide();
            var contentClass = $(this).attr('id');

            $('.' + contentClass + '-content').show();
        });

        $(document).on('click', '.digicon .poll-content .choices-list button', function(event) {
            event.preventDefault();

            var vote = $(this).val();

            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');

            $('#voteCasted').val(vote);
        });

        $(document).on('click', '.digicon .poll-content .button a', function(event) {
            event.preventDefault();

            var vote = $('#voteCasted').val();
            getDigiconPoll(vote);
            $(this).parent().hide();
        });

        $(document).on('click', '.dropdown input', function(event) {
            event.preventDefault();

            $(this).parent().parent().toggleClass('opened');
            $(this).parent().parent().find('ul').slideToggle('fast');
        });

        $(document).on('click', '.dropdown label', function(event) {
            event.preventDefault();

            $(this).parent().toggleClass('opened');
            $(this).parent().find('ul').slideToggle('fast');
        });

        $(document).on('click', '.dropdown .items li a', function(event) {
            event.preventDefault();

            var selected = $(this).text();

            $(this).parent().parent().parent().find('input').attr('value', selected);
            $(this).parent().parent().parent().find('input').val(selected);
            $(this).parent().parent().parent().toggleClass('opened');
            $(this).parent().parent().slideToggle('fast');
            $(this).parent().parent().parent().addClass('active');

        });

        $(document).on('click', '.checkbox-placeholder', function(event) {
            event.preventDefault();

            $(this).toggleClass('checked');
            $(this).parent().find('input[type=checkbox]').trigger('click');
        });

        $(document).on('click', '.form-container label', function(event) {
            event.preventDefault();

            $(this).parent().find('input').focus();
        });

        $(document).on('click', '.form-container.upload .input-upload, .form-container.upload label', function(event) {
            event.preventDefault();

            $(this).parent().find('.input-upload-file').trigger('click');
        });

        $(document).on('click', '.form-section-group .demo .remove', function(event) {
            event.preventDefault();

            $(this).parent().remove();
            for (var i = 1; i <= $('.demo-list .demo').length; i++) {
                $('.demo-list .demo:nth-child(' + i + ') .heading span').text(i);
            }
        });

        $(document).on('change', '.form-container.upload input[type=file]', function(event) {
            var filePath = $(this).parent().find('.input-upload-file').val().replace(/C:\\fakepath\\/i, '');

            $(this).parent().find('.input-upload').val(filePath);
        });

        $(document).on('focusin', '.form-container input', function(event) {
            event.preventDefault();
            /* Act on the event */
            $(this).parent().addClass('active');
        });

        $(document).on('focusout', '.form-container input', function(event) {
            event.preventDefault();
            /* Act on the event */
            if ($(this).val()) {

            } else {
                $(this).parent().removeClass('active');
            }
        });

        $(document).on('click', '.join-as ul li a', function(event) {
            event.preventDefault();
            var formType = $(this).parent().attr('value').toLowerCase();

            $('.submit-form .section-form').fadeOut('fast');
            $('.' + formType).fadeIn('fast');
        });

        $('.artists-gallery .item').hover(function() {
            $(this).find('.details').slideToggle('fast');
            $(this).find('.tag').slideToggle('fast');
        }, function() {
            $(this).find('.details').slideToggle('fast');
            $(this).find('.tag').slideToggle('fast');
        });

        //FORM INSTRUMENT SECTION
        $(document).on('click', '.form-section-group .add-instrument', function(event) {
            event.preventDefault();
            var countInstruments = $('.instruments .instrument').length,
                currNum = parseInt($('.instruments .instrument-list .instrument:last-child .heading span').text()) + 1;
            $('.instruments .instrument-list').append('<div class="instrument">' +
                '<a href="#" class="btn btn-red invert remove">Remove</a>' +
                '<p class="heading">Instrument #<span>' + currNum + '</span></p>' +
                '<div class="form-container dropdown required">' +
                '<div class="input-container">' +
                '<input type="text" name="instrument" placeholder="Instrument" readonly>' +
                '</div>' +
                '<ul class="items">' +
                '<li><a href="#">Instrument 1</a></li>' +
                '<li><a href="#">Instrument 2</a></li>' +
                '</ul>' +
                '</div>' +
                '</div>');
        });

        $(document).on('click', '.form-section-group .instrument .remove', function(event) {
            event.preventDefault();

            $(this).parent().remove();
            for (var i = 1; i <= $('.instrument-list .instrument').length; i++) {
                $('.instrument-list .instrument:nth-child(' + i + ') .heading span').text(i);
            }
        });

        //FORM DEMO SECTION
        $(document).on('click', '.demo .buttons a', function(event) {
            event.preventDefault();
            /* Act on the event */
            var type = $(this).attr('data-attr');

            $('.demo .buttons .btn-red').addClass('invert');
            $(this).removeClass('invert');
            $(this).parent().parent().find('.form-container').hide();
            $(this).parent().parent().find('.form-container.' + type).show();
        });

        $(document).on('click', '.form-section-group .add-demo', function(event) {
            event.preventDefault();
            var countInstruments = $(this).parent().parent().find('.demo').length,
                currNum = parseInt($(this).parent().parent().find('.demo:last-child .heading span').text()) + 1;
            $('.demos .demo-list').append('<div class="demo">' +
                '<a href="#" class="btn btn-red invert remove">Remove</a>' +
                '<p class="heading">Demo #<span>' + currNum + '</span></p>' +
                '<div class="buttons flex">' +
                '<a href="#" data-attr="link" class="btn btn-red">Link</a>' +
                '<a href="#" data-attr="file" class="btn btn-red invert">File</a>' +
                '</div>' +
                '<div class="form-container required link">' +
                '<input type="text" name="demo-link">' +
                '<label for="demo-link">Youtube / Soundcloud / Bandcamp link <span>*</span></label>' +
                '<p class="sub text-italic">Please give us one that best represents you and your genre.</p>' +
                '</div>' +
                '<div class="form-container required upload file">' +
                '<input type="file" class="demo-upload input-upload-file" name="upload">' +
                '<input type="text" name="demo-link" class="demo-upload input-upload" readonly>' +
                '<label for="upload">Upload a file of your demo <span>*</span></label>' +
                '<p class="sub text-italic">Please give us one that best represents you and your genre.</p>' +
                '</div> ' +
                '</div>');
        });
    }

    //FORM MEMBER SECTION
    $(document).on('click', '.form-section-group .add-member', function(event) {
        event.preventDefault();
        var countInstruments = $('.member-profile .member').length,
            currNum = parseInt($('.member-profile .members-list .member:last-child .heading span').text()) + 1;
        $('.member-profile .members-list').append('<div class="member">' +
            '<a href="#" class="btn btn-red invert remove">Remove</a>' +
            '<p class="heading">Member #<span>' + currNum + '</span></p>' +
            '<div class = "form-container required" >' +
            '<input type="text" name="full-name">' +
            '<label for="full-name">Full Name <span>*</span></label>' +
            '</div>' +
            '<div class="form-container dropdown required">' +
            '<div class="input-container">' +
            '<input type="text" name="gender" readonly>' +
            '</div>' +
            '<label for="gender">Gender <span>*</span></label>' +
            '<ul class="items">' +
            '<li><a href="#">Male</a></li>' +
            '<li><a href="#">Female</a></li>' +
            '</ul>' +
            '</div>' +
            '<div class="form-container required">' +
            '<input type="text" name="age">' +
            '<label for="age">Age <span>*</span></label>' +
            '</div>' +
            '<div class="form-container required">' +
            '<input type="text" name="role">' +
            '<label for="role">Role <span>*</span></label>' +
            '</div> ' +
            '</div>');
    });

    $(document).on('click', '.form-section-group .member .remove', function(event) {
        event.preventDefault();

        $(this).parent().remove();
        for (var i = 1; i <= $('.members-list .member').length; i++) {
            $('.members-list .member:nth-child(' + i + ') .heading span').text(i);
        }
    });

    $(document).on('click', '.follow .icon-follow', function(event) {
        event.preventDefault();

        $('.follow .links').slideToggle('fast');
        $(this).toggleClass('active');
    });

    $(document).on('click', '.discography-list .episode-block a, .upload-list .episode-block a', function(event) {
        event.preventDefault();

        var vidLink = $(this).attr('data-attr');

        $('.modal iframe').attr('src', vidLink);
        $('.modal').fadeIn('fast');
    });

    $(document).on('click', '.modal-close', function(event) {
        event.preventDefault();

        $('.modal').fadeOut('fast');
    });

    $(document).on('click', '.section-side .buttons .mode .go-dark', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('.section-side').toggleClass('dark');
        $('.section-side .buttons .mode').each(function(index, el) {
            $(this).find('.label').text('Light Mode');
            $(this).find('a').addClass('go-light');
            $(this).find('a').removeClass('go-dark');
        });
    });

    $(document).on('click', '.section-side .buttons .mode .go-light', function(event) {
        event.preventDefault();
        $('.section-side').toggleClass('dark');
        $('.section-side .buttons .mode').each(function(index, el) {
            $(this).find('.label').text('Dark Mode');
            $(this).find('a').addClass('go-dark');
            $(this).find('a').removeClass('go-light');
        });
    });

    $(document).on('click', '.show-description', function(event) {
        event.preventDefault();

        $('.description-container').slideToggle('fast');
        $(this).find('i').toggleClass('fa-caret-down');
        $(this).find('i').toggleClass('fa-caret-up');
    });

    $('.slider-for').on('swipe', function(event, slick, direction) {
        var dateVal = $('.slick-active .date').text();

        $('.whats-new > .container .date').text(dateVal);
    });

    $(document).on('click', '.section-side .tabs .tab', function(event) {
        event.preventDefault();

        $('.tabs .tab.active').removeClass('active');

        var activeTab = $(this).text().toLowerCase();

        $('.side-content, .article-inner .event-container').fadeOut('fast');
        $('.event-container').fadeOut('fast');

        if (activeTab == "events") {
            $('.event-container').fadeIn('fast');
        } else {
            $('.' + activeTab).fadeIn('fast');
        }
    });

    $(document).on('click', '.profile-page .tabs .tab', function(event) {
        event.preventDefault();

        var activeTab = $(this).text().toLowerCase().replace(/\s/g, '-');

        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');

        if (activeTab == 'my-playlists') {
            $('.read-list').fadeOut('fast');
            $('.playlists').fadeIn('fast');
        } else {
            $('.playlists').fadeOut('fast');
            $('.read-list').fadeIn('fast');
        }
    });

    $(document).on('click', '.digicon .live-content a', function(event) {
        event.preventDefault();
        /* Act on the event */

        var embed = $(this).attr('data-attr');
        $('.digicon .video iframe').attr('src', embed);
    });

    $(document).on('click', '.digicon .replay-content a', function(event) {
        event.preventDefault();
        /* Act on the event */

        var embed = $(this).attr('data-attr');
        $('.digicon .video iframe').attr('src', embed);

        $('.digicon .replay-content .active').removeClass('active');
        $(this).addClass('active');
    });

    $(document).on('click', '.digicon .replay-content .button a', function(event) {
        $('.digicon .tabs #live').click();
    });

    function getWhatsNew() {
        $.ajax({
            url: "/theme/scripts/data/whats-new.json",
            success: function(data) {
                var btnText = data.btnText;
                $('.date.mobile').text(data.whatsNew[0].date);
                $.each(data.whatsNew, function(index, val) {
                    $('.whats-new .slider-for').append('<div class="item">' +
                        '<div class="cover-photo">' +
                        '<img src="' + val.imgCover + '" alt="" class="cover">' +
                        '<img src="' + val.imgCard + '" alt="" class="card">' +
                        '</div>' +
                        '<div class="details">' +
                        '<div class="container">' +
                        '<p class="date">' + val.date + '</p>' +
                        '<div class="writeup">' +
                        '<p><span class="tag red inverted category"><a href="' + val.link + '">Read</a></span></p>' +
                        '<p class="title">' + val.title + '</p>' +
                        '<p class="desc">' + val.desc + '</p>' +
                        '</div>' +
                        '<div class="button">' +
                        '<a href="' + val.link + '" class="btn btn-default">' + btnText + '</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');

                    $('.whats-new .slider-nav .container').append('<div class="item">' +
                        '<img src="' + val.thumb + '" alt="">' +
                        '</div>');
                });
                whatsNewSlider();
            }
        });
    }

    function getSpotlight() {
        $.ajax({
            url: "/theme/scripts/data/spotlight.json",
            success: function(data) {
                $.each(data.spotlight, function(index, val) {
                    $('.spotlight .container > .flex').append('<div class="fl-4">' +
                        '<span class="tag ' + val.tag.toLowerCase() + '"><a href="find-artists.html">' + val.tag + '</a></span>' +
                        '<div class="thumb">' +
                        '<a href="' + val.link + '"><img src="' + val.thumb + '" alt=""></a>' +
                        '</div>' +
                        '<div class="details">' +
                        '<span class="tag red inverted category">' + val.category + '</span>' +
                        '<p class="title"><a href="' + val.link + '">' + val.title + '</a></p>' +
                        '<p class="desc">' + val.desc + '</p>' +
                        '</div>' +
                        '</div>');
                });
                spotlightSlider();
            }
        });
    }

    function getBeDiscovered() {
        $.ajax({
            url: "/theme/scripts/data/be-discovered.json",
            success: function(data) {
                var val = data.beDiscovered;
                $('.be-discovered').append('<picture class="cover">' +
                    '<source srcset="' + val.coverLg + '" media="(min-width: 769px)">' +
                    '<img srcset="' + val.coverSm + '">' +
                    '</picture>' +
                    '<div class="writeup">' +
                    '<div class="container">' +
                    '<p class="heading">' + val.heading + '</p>' +
                    '<p class="desc">' + val.sub + '</span></p>' +
                    '<div class="button">' +
                    '<a href="be-discovered-submit.html" class="btn btn-red">' + val.btnText + '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            }
        });
    }

    function getMustWatch() {
        $.ajax({
            url: "/theme/scripts/data/must-watch.json",
            success: function(data) {
                var featured = data.mustWatch.featured,
                    otherEpisodes = data.mustWatch.otherEpisodes;

                // FEATURED
                $('.homepage .must-watch .featured').append('<span class="tag red"><a href="' + featured.tagLink + '">' + featured.tag + '</a></span>' +
                    '<div class="thumb">' +
                    '<a href="' + featured.videoLink + '"><img src="' + featured.thumb + '" alt=""></a>' +
                    '</div>' +
                    '<div class="details">' +
                    '<p class="playlist"><a href="' + featured.videoLink + '">' + featured.playlist + '</a></p>' +
                    '<p class="title"><a href="' + featured.videoLink + '">' + featured.title + '</a></p > ' +
                    '<p class="desc">' + featured.desc + '</p>' +
                    '</div>');

                // OTHER EPISODES
                $.each(otherEpisodes, function(index, val) {
                    $('.homepage .must-watch .other-episodes').append('<div class="episode-block flex">' +
                        '<div class="thumb">' +
                        '<a href="' + val.videoLink + '"><img src="' + val.thumb + '" alt=""></a>' +
                        '</div>' +
                        '<div class="details">' +
                        '<span class="tag red"><a href="' + val.tagLink + '">' + val.tag + '</a></span>' +
                        '<p class="playlist"><a href="' + val.videoLink + '">' + val.playlist + '</a></p>' +
                        '<p class="title"><a href="' + val.videoLink + '">' + val.title + '</a></p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getHomePlaylists() {
        $.ajax({
            url: "/theme/scripts/data/home-playlists.json",
            success: function(data) {
                $.each(data.homePlaylists, function(index, val) {
                    $('.homepage .playlists').append('<div class="item">' +
                        '<div class="watermark">' +
                        '<a href="' + val.link + '"><img src="theme/images/watermark-youtube.png" alt=""></a>' +
                        '</div>' +
                        '<div class="cover">' +
                        '<a href="' + val.link + '">' +
                        '<picture>' +
                        '<source srcset="' + val.coverLg + '" media="(min-width: 768px)">' +
                        '<img srcset="' + val.coverSm + '" alt="">' +
                        '</picture>' +
                        '</a>' +
                        '</picture>' +
                        '</div>' +
                        '<div class="details">' +
                        '<p class="title"><a href="' + val.link + '">' + val.title + '</a></p>' +
                        '<p class="videos"><span>' + val.videoCount + '</span> Videos</p>' +
                        '<p class="artists">' + val.artists + '</p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getFeaturedEvents() {
        $.ajax({
            url: "/theme/scripts/data/featured-events.json",
            success: function(data) {
                $.each(data.featuredEvents, function(index, val) {
                    $('.events .featured-events').append('<div class="item flex">' +
                        '<div class="date">' +
                        '<p class="day">' + val.day + '</p>' +
                        '<p class="month">' + val.month + '</p>' +
                        '</div>' +
                        '<div class="details">' +
                        '<p class="title">' + val.title + '</p>' +
                        '<p class="desc">' + val.address + '</p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getMustRead() {
        $.ajax({
            url: "/theme/scripts/data/must-read.json",
            success: function(data) {
                $.each(data.mustRead, function(index, val) {
                    $('.must-read .articles').append('<div class="item flex">' +
                        '<div class="thumb">' +
                        '<a href="' + val.link + '"><img src="' + val.thumb + '" alt=""></a>' +
                        '</div>' +
                        '<div class="details">' +
                        '<p class="title"><a href="' + val.link + '">' + val.title + '</a></p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getDontMissOut() {
        $.ajax({
            url: "/theme/scripts/data/dont-miss-out.json",
            success: function(data) {
                var content = data.dontMissOut;

                $('.dont-miss-out').append('<picture class="cover">' +
                    '<source srcset="' + content.coverLg + '" media="(min-width: 769px)">' +
                    '<img srcset="' + content.coverSm + '" alt="…">' +
                    '</picture>' +
                    '<div class="writeup">' +
                    '<div class="container">' +
                    '<p class="heading">' + content.heading + ' </p>' +
                    '<p class="desc">' + content.desc + '</p>' +
                    '<div class="button">' +
                    '<a href="#" class="btn btn-red">Subscribe Now</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            }
        });
    }

    function getBeDiscoveredSplash() {
        $.ajax({
            url: "/theme/scripts/data/be-discovered-splash.json",
            success: function(data) {
                var content = data.beDiscoveredSplash;

                $('.splash-be-discovered').append('<picture class="cover">' +
                    '<source srcset="' + content.coverLg + '" media="(min-width: 600px)">' +
                    '<img srcset="' + content.coverSm + '" alt="…">' +
                    '</picture>' +
                    '<div class="container">' +
                    '<p class="heading">' + content.heading + '</p>' +
                    '<div class="writeup">' + content.desc + '</div>' +
                    '</div>');
            }
        });
    }

    function getSuccessStories() {
        $.ajax({
            url: "/theme/scripts/data/success-stories.json",
            success: function(data) {
                $.each(data.successStories, function(index, val) {
                    $('.success-stories .stories').append('<div class="item">' +
                        '<div class="thumb">' +
                        '<img src="' + val.thumb + '" alt="">' +
                        '</div>' +
                        '<img src="theme/images/icon-quote.png" class="quote" alt="">' +
                        '<div class="writeup">' +
                        '<p>' + val.desc + '</p>' +
                        '</div>' +
                        '<div class="meta-artist">' +
                        '<p class="artist">' + val.artist + '</p>' +
                        '<p class="artist-type">' + val.artistCategory + '</p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getLivestream() {
        $.ajax({
            url: "/theme/scripts/data/livestream.json",
            success: function(data) {
                var content = data.livestream;

                $('.live-video .container').prepend('<picture class="cover">' +
                    '<source srcset="' + content.coverLg + '" media="(min-width: 769px)">' +
                    '<img srcset="' + content.coverSm + '" alt="…">' +
                    '</picture>');

                $('.live-video .container .episode-block .thumb').prepend(content.embed);
                $('.live-video .container .episode-block .thumb .top p').append(content.userCount);
                $('.live-video .writeup .title').text(content.title);
                $('.live-video .writeup .season').text('Season ' + content.season + ': Episode ' + content.seasonEpisode);
                $('.live-video .writeup .episode').text(content.episode);
            }
        });
    }

    function getDigiconLive() {
        $.ajax({
            url: "/theme/scripts/data/digicon.json",
            success: function(data) {
                var content = data.digicon;

                $('.digicon .video-container .brand-top').append(content.brand1);
                $('.digicon .video-container .brand-bottom').append(content.brand2);
                $('.digicon .video-container .video iframe').attr('src', content.embedDefault);
                $('.digicon .video-container .video .top p').append(content.userCount);
                $('.digicon .writeup .title').text(content.title);
                $('.digicon .writeup .season').text('Season ' + content.season + ': Episode ' + content.seasonEpisode);
                $('.digicon .writeup .episode').text(content.episode);

                //CAMERAS
                $.each(content.cameras, function(index, val) {
                    $('.digicon .live-content').append('<div class="episode-block flex">' +
                        '<div class="thumb">' +
                        '<a href="#" data-attr="' + val.embed + '"><img src="' + val.thumb + '" alt=""></a>' +
                        '</div>' +
                        '<div class="details">' +
                        '<p class="title"><a href="#" data-attr="' + val.embed + '">' + val.title + '</a></p>' +
                        '<p class="desc">' + val.desc + '</p>' +
                        '</div>' +
                        '</div>');
                });
            }
        });
    }

    function getDigiconReplay() {
        $.ajax({
            url: "/theme/scripts/data/digicon.json",
            success: function(data) {
                var content = data.digicon;

                // REPLAYS
                $('.digicon .replay-content .title').text(content.episode);
                $('.digicon .replay-content .button a').attr('data-attr', content.embedDefault);

                $.each(content.replays, function(index, val) {
                    $('.digicon .replay-list').append('<li class="item">' +
                        '<div class="flex">' +
                        '<a href="#" data-attr="' + val.embed + '">' + val.title + '</a>' +
                        '<p class="time">' + val.length + '</p>' +
                        '</div>' +
                        '</li>');
                });
                setTimeout(function() {
                    videoSide();
                }, 1000);
            }
        });
    }

    function getDigiconChat() {
        $.ajax({
            url: "/theme/scripts/data/digicon.json",
            success: function(data) {
                var content = data.digicon;

                $('.digicon .chat-content').append(content.chatEmbed);

            }
        });
    }

    function getDigiconPoll(vote) {
        if (vote) {
            $.ajax({
                url: "/theme/scripts/data/digicon.json",
                success: function(data) {
                    var content = data.digicon,
                        totalVotes = parseInt(content.poll.totalVotes),
                        percentage = 0;

                    $('.digicon .poll-content .question').text(content.poll.question);
                    $('.digicon .poll-content .choices-list').empty();

                    $.each(content.poll.choices, function(index, val) {
                        if (val.cid == vote) {
                            percentage = ((parseInt(val.votes)+1)/totalVotes)*100;

                            $('.digicon .poll-content .choices-list').append('<div class="progress active">' +
                                '<span class="value" style="width:'+percentage.toFixed(2)+'%;"></span>' +
                                '<p class="title">'+val.choice+' <span class="result">'+percentage.toFixed(2)+'%</span></p>' +
                                '</div>');
                        } else {
                            percentage = (parseInt(val.votes)/totalVotes)*100;

                            $('.digicon .poll-content .choices-list').append('<div class="progress">' +
                                '<span class="value" style="width:'+percentage.toFixed(2)+'%;"></span>' +
                                '<p class="title">'+val.choice+' <span class="result">'+percentage.toFixed(2)+'%</span></p>' +
                                '</div>');
                        }
                    });
                }
            });
        } else {
            $.ajax({
                url: "/theme/scripts/data/digicon.json",
                success: function(data) {
                    var content = data.digicon;

                    $('.digicon .poll-content .question').text(content.poll.question);

                    $.each(content.poll.choices, function(index, val) {
                        $('.digicon .poll-content .choices-list').append('<button class="btn btn-invert block" value="' + val.cid + '">' + val.choice + '</button>');
                    });
                }
            });
        }
    }

    function whatsNewSlider() {
        if ($(window).width() > 768) {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav .container',
                responsive: [{

                }]
            });
            $('.slider-nav .container').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                centerMode: false,
                focusOnSelect: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 2000
            });
        } else {
            $('.slider-for').slick({
                centerMode: true,
                centerPadding: '180px',
                slidesToShow: 1,
                arrows: false,
                dots: true,
                responsive: [{
                    breakpoint: 600,
                    settings: {
                        centerPadding: '50px'
                    }
                }, {
                    breakpoint: 580,
                    settings: {
                        centerPadding: '20px'
                    }
                }]
            });
        }
    }

    function spotlightSlider() {
        if ($(window).width() < 769) {
            $('.spotlight .flex').slick({
                centerMode: true,
                centerPadding: '180px',
                slidesToShow: 1,
                arrows: false,
                dots: true,
                responsive: [{
                    breakpoint: 600,
                    settings: {
                        centerPadding: '50px'
                    }
                }, {
                    breakpoint: 580,
                    settings: {
                        centerPadding: '20px'
                    }
                }]
            });
        } else {
            $('.spotlight .flex.slick-initialized').slick('unslick');
        }
    }

    function videoSide() {
        var totalHeight = $('.digicon.side-container .video-container .content').height(),
            topSidebarHeight = $('.digicon.side-container .side .writeup').height() + $('.digicon.side-container .side .tabs').height() + 95;

        $('.digicon.side-container .side .tab-contents').height(totalHeight - topSidebarHeight);
    }

    function gallerySlide() {
        $('.gallery-page .slider-gallery').each(function(index, el) {
            $(this).find('.slider-for:not(.slick-initialized)').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: $(this).find('.slider-nav'),
                responsive: [{
                    breakpoint: 769,
                    settings: {
                        slidesToScroll: 1,
                    }
                }]
            });
            $(this).find('.slider-nav:not(.slick-initialized)').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: $(this).find('.slider-for'),
                dots: true,
                centerMode: true,
                focusOnSelect: true,
                responsive: [{
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 2
                    }
                }]
            });
        });
    }

    function watchNowSlider() {
        $('.video-sliders .slider').each(function(index, el) {
            $(this).slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
            });
        });
    }

    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function resizeFunction() {
        if ($('.whats-new .slider-for').length > 0) {
            getWhatsNew();
        }
        if ($('.spotlight').length > 0) {
            getSpotlight();
        }
        if ($(window).width() < 769) {
            if ($('.whats-new').length > 0) {
                $('.must-read').insertAfter('.must-watch');

                $('.playlists .heading').text('Stream-worthy');
            }
            if ($('.event-section ').length > 0) {
                $('.event-section .navigation .label').text('OR');
            }
            if ($('.gallery').length > 0) {
                $('.gallery .article-list-container .button a').text('See all albums');
            }
            if ($('.artist-inner').length > 0) {
                $('.artist-inner .event-container').appendTo('.article-side');
            }

        } else {
            if ($('.whats-new').length > 0) {
                $('.must-read').prependTo('.sticky');

                $('.playlists .heading').text('Playlists');
            }
            if ($('.event-section ').length > 0) {
                $('.event-section .navigation .label').text('Monthly Browse');
            }
            if ($('.gallery').length > 0) {
                $('.gallery .article-list-container .button a').text('Load more albums');
            }
            if ($('.artist-inner').length > 0) {
                $('.artist-inner .event-container').appendTo('.article-content');
            }
        }
    }

    return {
        init: init
    };
}());